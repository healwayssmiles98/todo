-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: todo
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `JobName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreatedBy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` tinyint DEFAULT NULL,
  `Deleted` tinyint DEFAULT NULL,
  `FK_JobTypesId` int DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (1,'công việc 2','admin','2020-04-04 00:00:00','admin','2020-04-04 00:00:00','2020-05-02 00:00:00','2020-04-25 00:00:00','123','3213',0,1,3),(2,'123','admin','2020-04-04 00:00:00',NULL,NULL,'2020-04-18 00:00:00','2020-04-11 00:00:00','312','321',0,1,1),(3,'1234567890','admin','2020-04-04 00:00:00','admin','2020-04-04 00:00:00','2020-04-18 00:00:00','2020-04-25 00:00:00','123','321',2,1,3),(4,'312421421','admin','2020-04-04 00:00:00',NULL,NULL,'2020-04-25 00:00:00','2020-04-18 00:00:00','31','321',2,NULL,1),(5,'qưeqewqewq','admin','2020-04-04 00:00:00',NULL,NULL,'2020-04-18 00:00:00','2020-04-18 00:00:00','ewq','ewq',0,NULL,4),(6,'123421','admin','2020-04-04 00:00:00',NULL,NULL,'2020-04-25 00:00:00','2020-04-18 00:00:00','123','321',0,1,3),(7,'12341','admin','2020-04-04 00:00:00',NULL,NULL,'2020-04-18 00:00:00','2020-04-18 00:00:00','321','321',2,NULL,3),(8,'tú nguyễn duy','admin','2020-04-04 00:00:00',NULL,NULL,'2020-04-25 00:00:00','2020-04-25 00:00:00','123','123',1,NULL,2),(9,'12345123','admin','2020-04-04 00:00:00',NULL,NULL,'2020-04-25 00:00:00','2020-04-24 00:00:00','123','321',1,NULL,4),(10,'11q','admin','2020-04-04 00:00:00',NULL,NULL,'2020-04-18 00:00:00','2020-04-11 00:00:00','','12',0,NULL,4);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobtypes`
--

DROP TABLE IF EXISTS `jobtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobtypes` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `JobTypesName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobtypes`
--

LOCK TABLES `jobtypes` WRITE;
/*!40000 ALTER TABLE `jobtypes` DISABLE KEYS */;
INSERT INTO `jobtypes` VALUES (1,'Loại công việc 1','Không có mô tả'),(2,'Loại công việc 2','Không có mô tả'),(3,'Loại công việc 3','Không có mô tả'),(4,'Loại công việc 4','Không có mô tả');
/*!40000 ALTER TABLE `jobtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'todo'
--

--
-- Dumping routines for database 'todo'
--
/*!50003 DROP PROCEDURE IF EXISTS `JOBS_CREATE` */;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `JOBS_CREATE`(
	IN JobName NVARCHAR(200),
    IN StartDate DATETIME,
    IN EndDate DATETIME,
    IN Description NVARCHAR(1000),
    IN Content NVARCHAR(1000),
    IN FK_JobTypesId INT,
	OUT ErrorCode NVARCHAR(100),
    OUT ErrorMessage NVARCHAR(4000)
)
BEGIN
	DECLARE EXIT HANDLER FOR sqlexception
    BEGIN
		GET DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
        SET ErrorCode = @p1;
        SET ErrorMessage = @p2;
        ROLLBACK;
    END;
    START TRANSACTION;
    
    IF exists (SELECT J.Id FROM JOBS J WHERE J.JobName = JobName)
    THEN
		SET ErrorCode = '1';
		SET ErrorMessage = N'Đã tồn tại công việc.';
		ROLLBACK;
    ELSE 
			INSERT INTO JOBS 
		(
			JobName,
			CreatedBy,
			CreatedDate,
			StartDate,
			EndDate,
			Description,
			Content,
			Status,
			FK_JobTypesId
		)
		VALUES
		(
			JobName,
			N'admin',
			CURRENT_DATE(),
			StartDate,
			EndDate,
			Description,
			Content,
			0,
			FK_JobTypesId
		);
		
		COMMIT;
		SET ErrorCode = '0';
		SET ErrorMessage = N'';
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `JOBS_DELETE` */;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `JOBS_DELETE`(
	IN Id INt(5),
    OUT ErrorCode NVARCHAR(100),
    OUT ErrorMessage NVARCHAR(4000)
)
BEGIN
	DECLARE EXIT HANDLER FOR sqlexception
    BEGIN
		GET DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
        SET ErrorCode = @p1;
        SET ErrorMessage = @p2;
        ROLLBACK;
    END;
    START TRANSACTION;
    UPDATE JOBS SET
			Deleted = 1 WHERE JOBS.Id = Id;
	COMMIT;
	SET ErrorCode = '0';
	SET ErrorMessage = N'';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `JOBS_GET_ALL_AND_SEARCH_PAGING` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `JOBS_GET_ALL_AND_SEARCH_PAGING`(
	IN PageIndex INT(2),
    IN PageSize INT(2),
    IN InWhere VARCHAR(100),
    OUT TotalRecords INT(5),
    OUT ErrorCode NVARCHAR(100),
    OUT ErrorMessage NVARCHAR(4000)
)
BEGIN
    DECLARE queryString NVARCHAR(4000) DEFAULT N'';
	DECLARE EXIT HANDLER FOR sqlexception
    BEGIN
		GET DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
        SET ErrorCode = @p1;
        SET ErrorMessage = @p2;
    END;
    SELECT J.Id,
		J.JobName,
		J.CreatedBy,
		J.CreatedDate,
        J.StartDate,
        J.EndDate,
        J.Description,
        J.Content,
        J.Status,
        JT.JobTypesName
		FROM JOBS J JOIN JOBTYPES JT ON J.FK_JobTypesId = JT.Id WHERE (J.Deleted = 0 OR J.Deleted IS NULL) AND J.JobName LIKE CONCAT(N'%', InWhere, '%')  ORDER BY J.Id ASC LIMIT PageIndex, PageSize;
	SELECT COUNT(*) INTO TotalRecords FROM JOBS J WHERE (J.Deleted = 0 OR J.Deleted IS NULL) AND J.JobName LIKE CONCAT(N'%', InWhere, '%');
    SET ErrorCode = '0';
    SET ErrorMessage = N'Lấy dữ liệu thành công.';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `JOBS_GET_BY_ID` */;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `JOBS_GET_BY_ID`(
	IN Id INT(5),
	OUT ErrorCode NVARCHAR(100),
    OUT ErrorMessage NVARCHAR(4000)
)
BEGIN
	DECLARE EXIT HANDLER FOR sqlexception
    BEGIN
		GET DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
        SET ErrorCode = @p1;
        SET ErrorMessage = @p2;
        ROLLBACK;
    END;
    START TRANSACTION;
    /*SELECT Id,
			JobName,
			CreatedBy,
			CreatedDate,
            UpdatedBy,
            UpdatedDate,
			StartDate,
			EndDate,
			Description,
			Content,
			Status,
			FK_JobTypesId FROM JOBS WHERE JOBS.Id = Id AND (JOBS.Deleted = 0 OR JOBS.Deleted IS NULL); */
	SELECT J.Id,
		J.JobName,
		J.CreatedBy,
		J.CreatedDate,
        J.UpdatedBy,
        J.UpdatedDate,
        J.StartDate,
        J.EndDate,
        J.Description,
        J.Content,
        J.Status,
        J.FK_JobTypesId,
        JT.JobTypesName
		FROM JOBS J JOIN JOBTYPES JT ON J.FK_JobTypesId = JT.Id WHERE (J.Deleted = 0 OR J.Deleted IS NULL) AND J.Id = Id;
		COMMIT;
		SET ErrorCode = '0';
		SET ErrorMessage = N'';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `JOBS_UPDATE` */;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `JOBS_UPDATE`(
	IN Id INT(5),
	IN JobName NVARCHAR(200),
    IN StartDate DATETIME,
    IN EndDate DATETIME,
    IN Description NVARCHAR(1000),
    IN Content NVARCHAR(1000),
    IN FK_JobTypesId INT,
	OUT ErrorCode NVARCHAR(100),
    OUT ErrorMessage NVARCHAR(4000)
)
BEGIN
	DECLARE EXIT HANDLER FOR sqlexception
    BEGIN
		GET DIAGNOSTICS CONDITION 1 @p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
        SET ErrorCode = @p1;
        SET ErrorMessage = @p2;
        ROLLBACK;
    END;
    START TRANSACTION;
    UPDATE JOBS SET
			JobName = JobName,
			UpdatedBy = 'admin',
			UpdatedDate = CURRENT_DATE(),
			StartDate = StartDate,
			EndDate = EndDate,
			Description = Description,
			Content = Content,
			FK_JobTypesId = FK_JobTypesId WHERE JOBS.Id = Id;
		COMMIT;
		SET ErrorCode = '0';
		SET ErrorMessage = N'';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `JOBTYPES_GET_ALL` */;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `JOBTYPES_GET_ALL`(
)
BEGIN
	SELECT j.Id, j.JobTypesName FROM jobtypes j;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `todo` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-04 16:41:14
