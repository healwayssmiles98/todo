<?php declare(strict_types = 1);
    class DbProvider {
        //CONST dbName = "todo";
        //CONST host = "localhost";
        CONST user = "root";
        CONST password = "123456";
        private $conn;
        function __construct()
        {
            $this->conn = new PDO("mysql:host=127.0.0.1;dbname=todo", self::user, self::password);
        }

        public function getConnection () {
            return $this->conn;
        }
    }
?>
