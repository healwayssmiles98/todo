<?php declare(strict_types = 1);
    require "./common/DbProvider.php";
    require "./models/JobTypes.php";
    require "./models/Job.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo App</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/animate.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="assets/css/select2.min.css" />
    <link rel="stylesheet" href="assets/css/jquery-confirm.min.css" />
    <style>
        .center {
            text-align: center;
        }
    </style>
</head>
<body>
    <script type="text/javascript" src="assets/js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/notify.min.js"></script>
    <script type="text/javascript" src="assets/js/select2.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/js/additional-methods.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-confirm.min.js"></script>
    <main>
        <div class="container top">
            <div class="row justify-content-center">
                <h4>Todo App</h4>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-10">
                <div class="card top">
                    <div class="card-header row justify-content-center clear-margin">

                        <div class="input-group col-lg-9">
                            <form method="get" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>" id="formSearch" style="width: 100%;">
                                <div>
                                    <input type="text" class="form-control" placeholder="Tìm kiếm công việc..." name="text" id="text" value="<?php if (isset($_GET['text'])) { echo $_GET['text']; } else { echo ""; } ?>" />
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title"><b>Danh sách công việc:</b> <button class="btn btn-sm btn-primary pull-right" id="create" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-circle"></i> Thêm mới công việc</button></h5>
                        <table class="table table-striped border-table">
                            <thead>
                            <tr>
                                <th scope="col" class="center">STT</th>
                                <th scope="col" class="center">Tên công việc</th>
                                <th scope="col" class="center">Loại công việc</th>
                                <th scope="col" class="center">Mô tả</th>
                                <th scope="col" class="center">Nội dung</th>
                                <th scope="col" class="center">Trạng thái</th>
                                <th scope="col" class="center">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            try {
                                if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                                    $db1 = new DbProvider();
                                    $conn1 = $db1->getConnection();
                                    // initial page when first load
                                    $pageIndex = 1;
                                    $pageSize = 5;
                                    $inWhere = "";
                                    if (isset($_GET['page'])) {
                                        $pageIndex = $_GET['page'];
                                    }
                                    if (isset($_GET['pageSize'])) {
                                        $pageSize = $_GET['pageSize'];
                                    }
                                    if (isset($_GET['text'])) {
                                        $inWhere = $_GET['text'];
                                    }
                                    $pageIndex = ($pageIndex - 1) * $pageSize;
                                    $query1 = "CALL `todo`.`JOBS_GET_ALL_AND_SEARCH_PAGING`({$pageIndex}, {$pageSize}, '{$inWhere}', @total, @outcode, @outmess)";
                                    $result1 = $conn1->query($query1);
                                    $result1->setFetchMode(PDO::FETCH_CLASS, "Job");
                                    //$result1->execute();
                                    //$result1->closeCursor();
                                }

                            }
                            catch (Exception $ex) {
                                echo $ex->getMessage();
                            } finally {
                                //$conn1 = null;
                            }
                            ?>
                            <?php while ($item = $result1->fetch()) { ?>
                            <tr>
                                <th scope="row" class="center"><?php echo $item->Id ?></th>
                                <td class="center"><?php echo $item->JobName ?></td>
                                <td class="center"><?php echo $item->JobTypesName ?></td>
                                <td class="center"><?php echo $item->Description ?></td>
                                <td class="center"><?php echo $item->Content ?></td>
                                <td class="center"><?php
                                        switch ($item->Status) {
                                            case 0: {
                                                echo "<span class='badge badge-secondary pointer status' status='0' job-id='$item->Id'>Vừa tạo</span>";
                                                break;
                                            }
                                            case 1: {
                                                echo "<span class='badge badge-info pointer status' status='1' job-id='$item->Id'>Đang thực hiện</span>";
                                                break;
                                            }
                                            case 2: {
                                                echo "<span class='badge badge-success pointer status' status='2' job-id='$item->Id'>Đã hoàn thành</span>";
                                                break;
                                            }
                                        }
                                    ?></td>
                                <td style="text-align: center;"><a href="crud/edit.php?id=<?php echo $item->Id ?>"><i class="fa fa-edit pointer"></i></a>&nbsp;&nbsp;<a href="crud/delete.php?id=<?php echo $item->Id ?>"><i class="fa fa-times pointer"></i></a></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="card-title">
                            <div>
                                <label style="margin: 0px;">Số bản ghi hiển thị: </label>
                                <select class="custom-control custom-select custom-select-sm" id="pages" style="width: 10%; margin-left: 15px;">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                </select>
                                <span class="badge badge-info pull-right" style="margin-top: 12px;">Tổng số: <?php
                                    $result1->closeCursor();
                                    $row = $conn1->query("SELECT @total AS TotalRecords")->fetch(PDO::FETCH_ASSOC);
                                    if ($row) {
                                        echo $row !== false ? $row['TotalRecords'] : null;
                                        //echo var_dump($row['TotalRecords']);
                                    }
                                    ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="padding-bottom: 3px;">
                        <nav aria-label="...">
                            <ul class="pagination" style="margin-bottom: 0px;">
                                <!-- <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active">
                                  <span class="page-link">
                                    2
                                    <span class="sr-only">(current)</span>
                                  </span>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                 -->
                                <?php
                                    //echo $pageSize;
                                    $totalRows = (int)$row['TotalRecords'];
                                    $pageNumber = 0;
                                    if ($totalRows > 0) {
                                        $pageNumber = $totalRows % $pageSize > 0 ? (int)(($totalRows / $pageSize) + 1) : $totalRows / $pageSize;
                                        $i = 1;
                                        while ($i <= $pageNumber) {

                                    ?>
                                            <li class="page-item"><a class="page-link" page="<?php echo $i; ?>" href="<?php
                                                if (isset($_GET['text']))
                                                {
                                                    echo "?page={$i}&pageSize={$pageSize}&text={$inWhere}";
                                                }
                                                else {
                                                    echo "?page={$i}&pageSize={$pageSize}";
                                                }

                                                ?>"><?php echo $i; ?></a></li>
                                            <?php $i++; ?>
                                <?php } } ?>
                            </ul>
                        </nav>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </main>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thêm mới công việc</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?php echo htmlspecialchars('./crud/create.php') ?>" id="formCreate">

                        <div class="form-group">
                            <label for="job-name" class="col-form-label">Tên công việc:</label>
                            <input type="text" class="form-control" id="job-name" name="jobname" required />
                        </div>
                        <div class="form-group">
                            <label for="job-types" class="col-form-label">Loại công việc:</label>
                            <select class="form-control custom-control custom-select" name="types">
                                <?php
                                global $option;
                                try {
                                    $db = new DbProvider(); // khởi tạo kết nối đến db
                                    $conn = $db->getConnection();
                                    $query = "CALL `todo`.`JOBTYPES_GET_ALL`()";
                                    $result = $conn->query($query);
                                    $result->setFetchMode(PDO::FETCH_CLASS, "JobTypes");

                                    while ($item = $result->fetch()) {
                                        //$GLOBALS['option'] += `<option value="{$item->Id}">{$item->JobTypesName}</option>`;
                                        echo "<option value='$item->Id'>$item->JobTypesName</option>";
                                    }
                                    //echo $GLOBALS['option'];
                                }
                                catch (Exception $ex){
                                    echo $ex->getMessage();
                                }
                                finally {
                                    //$conn = null;
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start-date" class="col-form-label">Thời gian bắt đầu:</label>
                            <input class="form-control" type="date" id="start-date" name="startdate" required/>
                        </div>
                        <div class="form-group">
                            <label for="end-date" class="col-form-label">Thời gian kết thúc:</label>
                            <input class="form-control" type="date" id="end-date" name="enddate" required/>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-form-label">Mô tả:</label>
                            <textarea class="form-control auto-resize" id="description" name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="content" class="col-form-label">Nội dung:</label>
                            <textarea class="form-control auto-resize" id="content" name="content" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right" id="submit-job">Tạo mới</button>
                        <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal" style="margin-right: 10px;">Hủy bỏ</button>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
           /* if (<?php //echo $_GET['error'] ?>) {
                $.notify("Thêm mới thất bại", { className: "error", position: "top center" });
            }
            else {
                $.notify("Thêm mới thành công", { className: "success", position: "top center" });
            } */
        $(document).find('#pages option').each(function () {
            if ($(this).val() == <?php if (isset($_GET['pageSize'])) { echo $_GET['pageSize']; } else { echo 5; }  ?>) {
                $(this).attr('selected', 'selected');
                return;
            }
        });
        const location = window.location.href;
        let strUrl = location.match(/page=\d+/);
        if (strUrl != null) {
            let page = strUrl[0].split('=')[1];
            let pageElements = $(document).find('.page-link');
            //pageElements.each((value, index) => {
            //    console.log($(this).attr('page'));
            //})
            $(document).find("[page=" + page + "]").closest(".page-item").addClass("active");
        }
        else {
            $(document).find("[page=1]").closest(".page-item").addClass("active");
        }

        $('#search').validate({
           rule: {
               text: {
                   required: true
               }
           },
            messages: {
               text: "Vui lòng nhập vào tên hoặc một phần tên của công việc."
            },
            submitHandler: function (form) {

            }
        });

        $("#formCreate").validate({
            rule: {
                jobname: {
                    required: true
                },
                startdate: {
                    required: true
                },
                enddate: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            messages: {
                jobname: "Vui lòng nhập tên công việc.",
                startdate: "Chọn ngày bắt đầu công việc.",
                enddate: "Chọn ngày kết thúc công việc.",
                content: "Nhập nội dung công việc."
            },
            submitHandler: function (form) {
                // code
                $('#exampleModal').modal('toggle');
                setTimeout(function () {
                    form.submit();
                }, 1000);
            }
        });

        $(document).on('change', '#pages', function () {
            //let option = $(document).find('#pages option:selected');
            //option.attr('selected', 'selected');
            <?php if (isset($_GET['text'])) { ?>
                window.location.href = "?page=1&pageSize=" + this.value + "&text=<?php echo $_GET['text'] ?>";
            <?php } else { ?>
                window.location.href = window.location.href = "?page=1&pageSize=" + this.value;
            <?php  } ?>
        });

        $(document).on('click', '.status', function () {
            const status = $(this).attr('status');
            const id = $(this).attr('job-id');
            $.confirm({
                title: 'Bạn có muốn thay đổi trạng thái công việc ?',
                content: 'Cửa sổ sẽ tự đóng trong 3 giây nếu bạn không phản hồi.',
                autoClose: 'Cancel|3000',
                theme: 'material',
                type: 'blue',
                buttons: {
                    changeStatus: {
                        text: 'Có, tôi muốn thay đổi',
                        keys: ['enter'],
                        action: function () {
                            setTimeout(function () {
                                window.location.href= "./crud/change_status.php?status=" + status + "&id=" + id;
                            }, 1000);
                        }
                    },
                    Cancel: function () {

                    }
                }
            });
        });
    })
</script>
</html>