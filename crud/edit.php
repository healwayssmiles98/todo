<?php declare(strict_types = 1);
    require "../common/DbProvider.php";
    require "../models/JobTypes.php";
    require "../models/Job.php";

    try {
        $db = new DbProvider();
        $conn = $db->getConnection();
        $id = $_GET['id'];
        $query = "CALL `todo`.`JOBS_GET_BY_ID`({$id}, @outcode, @outmess);";
        $result = $conn->query($query);
        $result->setFetchMode(PDO::FETCH_CLASS, "Job");

    }
    catch (Exception $ex) {
        echo $ex->getMessage();
    } finally {
        $conn = null;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo App</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/css/animate.css" />
    <link rel="stylesheet" href="../assets/css/style.css" />
    <link rel="stylesheet" href="../assets/css/select2.min.css" />

</head>
<body>
<script type="text/javascript" src="../assets/js/jquery-3.4.1.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/notify.min.js"></script>
<script type="text/javascript" src="../assets/js/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../assets/js/additional-methods.min.js"></script>

<main>
    <div class="container top">
        <div class="row justify-content-center">
            <h4>Update Job</h4>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card top">
                    <div class="card-header row justify-content-center clear-margin">

                    </div>
                    <div class="card-body">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Cập nhật công việc</h5>
                                </div>
                                <div class="modal-body">
                                    <?php
                                    if ($_SERVER['REQUEST_METHOD'] === "POST") {
                                        $db = new DbProvider();
                                        $connection = $db->getConnection();
                                        $updateQuery = $connection->prepare("CALL `todo`.`JOBS_UPDATE`(:id, :name, :startdate, :enddate, :description, :content, :type, @outcode, @outmess)");
                                        // set value for variable

                                        $updateQuery->bindParam(":id", $id);
                                        $updateQuery->bindParam(":name", $name);
                                        $updateQuery->bindParam(":startdate", $startdate);
                                        $updateQuery->bindParam(":enddate", $enddate);
                                        $updateQuery->bindParam(":description", $description);
                                        $updateQuery->bindParam(":content", $content);
                                        $updateQuery->bindParam(":type", $type);

                                        $id = $_GET['id'];
                                        $name = $_POST['jobname'];
                                        $startdate = $_POST['startdate'];
                                        $enddate = $_POST['enddate'];
                                        $description = $_POST['description'];
                                        $content = $_POST['content'];
                                        $type = $_POST['types'];
                                        $updateQuery->execute();

                                        $updateQuery->closeCursor();

                                        $errorMessage = $connection->query("SELECT @outmess AS ErrorMessage")->fetch(PDO::FETCH_ASSOC);
                                        if ($errorMessage) {
                                            if ($errorMessage['ErrorMessage'] === "") {
                                                header("location: ../index.php?error=false");
                                            }
                                            else {
                                                echo "Lỗi: <b>{$errorMessage['ErrorMessage']}</b>";
                                                header("location: ../index.php?error=true");
                                            }
                                        }
                                    }
                                    ?>
                                    <?php if ($result) { while ($job = $result->fetch()) { ?>
                                    <form method="post" action="/todo_app/crud/edit.php?id=<?php echo $job->Id ?>" id="formCreate">

                                        <div class="form-group">
                                            <label for="job-name" class="col-form-label">Tên công việc:</label>
                                            <input type="text" class="form-control" id="job-name" name="jobname" value="<?php echo $job->JobName ?>" required />
                                        </div>
                                        <div class="form-group">
                                            <label for="job-types" class="col-form-label">Loại công việc:</label>
                                            <select class="form-control custom-control custom-select" name="types">
                                                
                                                <?php
                                                global $option;
                                                try {
                                                    $db = new DbProvider(); // khởi tạo kết nối đến db
                                                    $conn = $db->getConnection();
                                                    $query = "CALL `todo`.`JOBTYPES_GET_ALL`()";
                                                    $result = $conn->query($query);
                                                    $result->setFetchMode(PDO::FETCH_CLASS, "JobTypes");

                                                    while ($item = $result->fetch()) {
                                                        if ($item->Id === $job->FK_JobTypesId) {
                                                            echo "<option value='$item->Id' selected>$item->JobTypesName</option>";
                                                        }
                                                        else {
                                                            echo "<option value='$item->Id'>$item->JobTypesName</option>";
                                                        }
                                                    }
                                                    //echo $GLOBALS['option'];
                                                }
                                                catch (Exception $ex){
                                                    echo $ex->getMessage();
                                                }
                                                finally {
                                                    //$conn = null;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="start-date" class="col-form-label">Thời gian bắt đầu:</label>
                                            <input class="form-control" type="date" id="start-date" name="startdate" value="<?php if (isset($job->StartDate)) { echo substr($job->StartDate, 0, 10); } ?>" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="end-date" class="col-form-label">Thời gian kết thúc:</label>
                                            <input class="form-control" type="date" id="end-date" name="enddate" value="<?php if (isset($job->EndDate)) { echo substr($job->EndDate, 0, 10); } ?>" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="description" class="col-form-label">Mô tả:</label>
                                            <textarea class="form-control auto-resize" id="description" name="description"><?php echo $job->Description ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="content" class="col-form-label">Nội dung:</label>
                                            <textarea class="form-control auto-resize" id="content" name="content" required><?php echo $job->Content ?></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary pull-right" id="submit-job">Cập nhật</button>
                                    </form>
                                    <?php }} ?>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

</body>
<script type="text/javascript">
    $(document).ready(function () {
        $("#formCreate").validate({
            rule: {
                jobname: {
                    required: true
                },
                startdate: {
                    required: true
                },
                enddate: {
                    required: true
                },
                content: {
                    required: true
                }
            },
            messages: {
                jobname: "Vui lòng nhập tên công việc.",
                startdate: "Chọn ngày bắt đầu công việc.",
                enddate: "Chọn ngày kết thúc công việc.",
                content: "Nhập nội dung công việc."
            }
        });
    })
</script>
</html>


