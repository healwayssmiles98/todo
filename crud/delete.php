<?php declare(strict_types = 1);
require "../common/DbProvider.php";
require "../models/JobTypes.php";
require "../models/Job.php";

try {
    $db = new DbProvider();
    $conn = $db->getConnection();
    $id = $_GET['id'];
    $query = "CALL `todo`.`JOBS_GET_BY_ID`({$id}, @outcode, @outmess);";
    $result = $conn->query($query);
    $result->setFetchMode(PDO::FETCH_CLASS, "Job");

}
catch (Exception $ex) {
    echo $ex->getMessage();
} finally {
    $conn = null;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo App</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../assets/css/animate.css" />
    <link rel="stylesheet" href="../assets/css/style.css" />
    <link rel="stylesheet" href="../assets/css/select2.min.css" />
    <link rel="stylesheet" href="../assets/css/jquery-confirm.min.css" />
</head>
<body>
    <script type="text/javascript" src="../assets/js/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/notify.min.js"></script>
    <script type="text/javascript" src="../assets/js/select2.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../assets/js/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery-confirm.min.js"></script>

<main>
    <div class="container top">
        <div class="row justify-content-center">
            <h4>Delete Job</h4>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card top">
                    <div class="card-header row justify-content-center clear-margin">

                    </div>
                    <div class="card-body">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Xóa công việc</h5>
                                </div>
                                <div class="modal-body">
                                    <?php
                                    if ($_SERVER['REQUEST_METHOD'] === "POST") {
                                        $db = new DbProvider();
                                        $connection = $db->getConnection();
                                        $updateQuery = $connection->prepare("CALL `todo`.`JOBS_DELETE`(:id, @outcode, @outmess)");
                                        $updateQuery->bindParam(":id", $id);
                                        $id = $_GET['id'];
                                        $updateQuery->execute();
                                        $updateQuery->closeCursor();
                                        $errorMessage = $connection->query("SELECT @outmess AS ErrorMessage")->fetch(PDO::FETCH_ASSOC);
                                        if ($errorMessage) {
                                            if ($errorMessage['ErrorMessage'] === "") {
                                                header("location: ../index.php?error=false");
                                            }
                                            else {
                                                echo "Lỗi: <b>{$errorMessage['ErrorMessage']}</b>";
                                                header("location: ../index.php?error=true");
                                            }
                                        }
                                    }
                                    ?>
                                    <?php if ($result) { while ($job = $result->fetch()) { ?>
                                        <form method="post" action="/todo_app/crud/delete.php?id=<?php echo $job->Id ?>" id="formDelete">

                                            <div class="form-group">
                                                <label for="job-name" class="col-form-label">Tên công việc:</label>
                                                <label class="col-form-label"><b><?php echo $job->JobName ?></b></label>
                                            </div>
                                            <div class="form-group">
                                                <label for="job-types" class="col-form-label">Loại công việc:</label>
                                                <label class="col-form-label"><b><?php echo $job->JobTypesName ?></b></label>
                                            </div>
                                            <div class="form-group">
                                                <label for="start-date" class="col-form-label">Thời gian bắt đầu:</label>
                                                <label class="col-form-label"><b><?php if (isset($job->StartDate)) { echo substr($job->StartDate, 0, 10); } ?></b></label>
                                            </div>
                                            <div class="form-group">
                                                <label for="end-date" class="col-form-label">Thời gian kết thúc:</label>
                                                <label class="col-form-label"><b><?php if (isset($job->EndDate)) { echo substr($job->EndDate, 0, 10); } ?></b></label>
                                            </div>
                                            <div class="form-group">
                                                <label for="description" class="col-form-label">Mô tả:</label>
                                                <label class="col-form-label"><b><?php echo $job->Description ?></b></label>
                                            </div>
                                            <div class="form-group">
                                                <label for="content" class="col-form-label">Nội dung:</label>
                                                <label class="col-form-label"><b><?php echo $job->Content ?></b></label>
                                            </div>

                                        </form>
                                    <?php }} ?>
                                    <button class="btn btn-primary pull-right" id="delete">Xóa</button>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

</body>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '#delete', function (e) {
            $.confirm({
                title: 'Bạn có chắc muốn xóa công việc ?',
                content: 'Cửa sổ sẽ tự đóng trong 6 giây nếu bạn không phản hồi.',
                autoClose: 'Cancel|6000',
                theme: 'material',
                type: 'blue',
                buttons: {
                    deleteJob: {
                        text: 'Xóa',
                        keys: ['enter'],
                        action: function () {
                            $.confirm({
                                title: 'Thông báo',
                                content: 'Xóa thành công, tải lại sau 3s',
                                autoClose: 'Cancel|3000',
                                theme: 'material',
                                type: 'blue',
                                buttons: {
                                    Cancel: function () {

                                    }
                                }
                            });
                            setTimeout(function () {
                                $('#formDelete').submit();
                            }, 3500);
                        }
                    },
                    Cancel: function () {

                    }
                }
            });
        });
    })
</script>
</html>


