<?php
    require '../common/DbProvider.php';
    require  '../models/Job.php';
    require  '../models/JobTypes.php';
    try {
        if ($_SERVER['REQUEST_METHOD'] === "POST") {
            $db = new DbProvider();
            $conn = $db->getConnection();
            $query = $conn->prepare("CALL `todo`.`JOBS_CREATE`(:name, :startdate, :enddate, :description, :content, :type, @outcode, @outmess)");
            // gán giá trị cho các placeholder
            $query->bindParam(":name", $name);
            $query->bindParam(":startdate", $startdate);
            $query->bindParam(":enddate", $enddate);
            $query->bindParam(":description", $description);
            $query->bindParam(":content", $content);
            $query->bindParam(":type", $type);

            // set value for variable
            $name = $_POST['jobname'];
            $startdate = $_POST['startdate'];
            $enddate = $_POST['enddate'];
            $description = $_POST['description'];
            $content = $_POST['content'];
            $type = $_POST['types'];

            $query->execute();
            $query->closeCursor();
            $errorMessage = $conn->query("SELECT @outmess AS ErrorMessage")->fetch(PDO::FETCH_ASSOC);
            if ($errorMessage) {
                if ($errorMessage['ErrorMessage'] === "") {
                    header("location: ../index.php?error=false");
                }
                else {
                    echo "Lỗi: <b>{$errorMessage['ErrorMessage']}</b>";
                    header("location: ../index.php?error=true");
                }
            }
        }
    }
    catch (Exception $ex) {
        echo $ex->getMessage();
    } finally {
        clearstatcache();
    }
?>
