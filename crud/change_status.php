<?php
    require '../common/DbProvider.php';
    require  '../models/Job.php';
    require  '../models/JobTypes.php';

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $dbStatus = new DbProvider();
        $conn = $dbStatus->getConnection();
        $query = $conn->prepare("UPDATE JOBS SET Status = :status WHERE Id = :id");
        $query->bindParam(":id", $id);
        $query->bindParam(":status", $status);

        $id = $_GET['id'];
        switch ($_GET['status']) {
            case 0: {
                $status = 1;
                break;
            }
            case 1: {
                $status = 2;
                break;
            }
            case 2: {
            $status = 0;
            break;
        }
        }
        $query->execute();

        header("location: ../index.php");
    }
?>