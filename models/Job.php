<?php declare(strict_types = 1);
    class Job {
        public $Id;
        public $JobName;
        public $CreatedBy;
        public $CreatedDate;
        public $UpdatedBy;
        public $UpdatedDate;
        public $StartDate;
        public $EndDate;
        public $Description;
        public $Content;
        public $Status;
        public $JobTypesName;
        public $FK_JobTypesId;
        public function  __construct()
        {

        }
    }
?>
